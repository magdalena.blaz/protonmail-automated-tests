# ProtonMail automated tests

Prerequisites: installed Google Chrome.

## Installing Katalon Recorder

Install Katalon Recorder (Selenium tests generator) extension, ver 5.5.4, following the instructions https://docs.katalon.com/katalon-studio/docs/katalon-addon-for-chrome.html

## Running the tests

1. Open Google Chrome.
1. Run Katalon Recorder.
1. Click Open test suite (ACTIONS section) -> Open KR Test Suite and select the KatalonRecorder.html file.
1. Click the ProtonMail test suite.
1. Click P.Suite to run test cases.



